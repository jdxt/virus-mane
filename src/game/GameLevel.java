package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

import java.util.*;

/**
 * GameWorld inits world characters and objects.
 */
public abstract class GameLevel extends World {
	private int n_enemies;
	private Player player;
	private Game game;
	private ArrayList<Enemy> enemies  = new ArrayList<>();


	public void populate(Game game, int enemies, Player.Gun gun) {
		this.setGravity(60f);
		n_enemies = enemies;
		this.game = game;

		// make a character
		player = new Player(this, gun);
		player.setPosition(new Vec2(Player.getHeight(), 0));
		player.addCollisionListener(new Collision(player));

		this.initEnemies();

		for (int i = 0; i < 2; i++) {
			Potion potion = new Potion(this, player);
			potion.setPosition(new Vec2(10+i*10,0));
		}
	}

	private void initEnemies() {
		for (int i = 0; i < n_enemies; i++) {
			Enemy enemy = new Enemy(this, Player.Gun.Pistol);
			enemy.setPosition(new Vec2(20+i * 20, 0));
			enemy.startWalking();
			enemy.addCollisionListener(new Collision(enemy));
			enemy.addDestructionListener(new Test());
			this.enemies.add(enemy);
		}
	}

	ArrayList<Enemy> getEnemies() {
		return this.enemies;
	}

	class Test implements DestructionListener {

		@Override
		public void destroy(DestructionEvent e) {
			if (e.getSource() instanceof Enemy) {
				n_enemies--;
			}
			if (n_enemies <= 0) {
				game.nextLevel();
			}
		}
	}

	public Player getPlayer() {
		return player;
	}
}
