package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.*;

/**
 * Virus Mane - 2D game.
 *
 * @author Jay Tauron
 * @version 1.0
 * @since 2020-02-01
 */
public class Game {

	// this flag sets the visibility of debug view
	private final static boolean DEBUG = false;
	private static State state = State.Level1;

	GameLevel world;

	/** Initialise a new Game.
	 * This instantiates the first GameLevel object,
	 * sets up a UserView into the world,
	 * and binds event listeners to the world.
	 */
	private void setWorld() {
		switch (state) {
			case Level1:
				world = new Level1(this);
				break;
			case Level2:
				System.out.println("level 2!");
				world = new Level1(this);
				break;
			case Level3:
				System.out.println("level 3!");
				world = new Level1(this);
				break;
			case End:
			default:
				world = new Level1(this);
				System.exit(0);
		}
	}

	public Game() {
		try {
			SoundClip c = new SoundClip("/home/jay/engine.wav");
			c.loop();
		} catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
			System.out.println(e);
		}
		this.setWorld();

		if (DEBUG) new DebugViewer(world, 500, 500);

		// init view
		UserView view = new UserView(world, 500, 500);
		view.setGridResolution(1);
		view.setCentre(new Vec2(1000, 500));

		// init frame
		final JFrame frame = new JFrame("Event handling");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(view);
		frame.setLocationByPlatform(true);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		frame.requestFocus();

		// Event listeners
		view.addMouseListener(new GiveFocus(frame));
		//view.addMouseListener(new MouseTest(world.getPlayer()));
		frame.addKeyListener(new Controller(world.getPlayer()));
		// Follow player
		world.addStepListener(new Tracker(view, world.getPlayer()));

		world.start();
	}

	public static void main(String[] args) {
		final Game game = new Game();
	}

	public void nextLevel() {
		state = state.next();
		this.setWorld();
	}

	enum State {
		Level1,
		Level2,
		Level3,
		End;

		public State next() {
			switch (this) {
				case Level1:
					return Level2;
				case Level2:
					return Level3;
				case Level3:
				case End:
				default:
					return End;
			}
		}
	}
}
