package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

public class Level1 extends GameLevel {

	Level1(Game game) {
		super.populate(game,2, Player.Gun.Pistol);

		// make the ground
		Shape groundShape = new BoxShape(99, 0.5f);
		StaticBody ground = new StaticBody(this, groundShape);
		ground.setPosition(new Vec2(0, 0));
		// left wall
		Shape leftWall = new BoxShape(0,50);
		StaticBody wall = new StaticBody(this, leftWall);
		wall.setPosition(new Vec2(-1, 0));

		this.addStepListener(new EnemyShooter());
	}

	private class EnemyShooter implements StepListener {

		@Override
		public void preStep(StepEvent stepEvent) {
			float PROXIMITY = 20;
			Vec2 pPos = Level1.this.getPlayer().getPosition();

			for (Enemy e : Level1.this.getEnemies()) {
				Vec2 ePos = e.getPosition();
				if (this.distance(pPos, ePos) > PROXIMITY)
					e.stopShooting();
				else if (!e.isShooting())
					e.startShooting();
			}
		}

		private double distance(Vec2 pPos, Vec2 ePos) {
			return Math.sqrt(pPos.sub(ePos).lengthSquared());
		}

		@Override
		public void postStep(StepEvent stepEvent) {}
	}

}
