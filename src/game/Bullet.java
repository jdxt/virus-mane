package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

public class Bullet extends DynamicBody {
	private static final Shape shape = new CircleShape(0.1f);
	private static final Vec2 offset = new Vec2(1.5f, 0);
	private static final Vec2 v = new Vec2(50, 0);
	private int damage;

	public Bullet(Player player, int damage, int i) {
		super(player.getWorld(), shape);
		this.damage = damage;
		this.setGravityScale(0);
		this.setBullet(true);
		this.addCollisionListener(new BulletCollision());

		float randY = (float)Math.random();
		Vec2 offset = Bullet.offset.add(new Vec2(0.5f*i, randY));

		switch (player.getDirection()) {
			case Left:
				this.setPosition(player.getPosition().add(new Vec2(offset.x * -1, offset.y)));
				this.setLinearVelocity(new Vec2(v.x * -1, v.y));
				break;
			case Right:
				this.setPosition(player.getPosition().add(offset));
				this.setLinearVelocity(v);
				break;
		}
	}

	public int getDamage() {
		return this.damage;
	}

	private static class BulletCollision implements CollisionListener {
		@Override
		public void collide(CollisionEvent e) {
			if (e.getReportingBody() instanceof Bullet) e.getReportingBody().destroy();
			if (e.getOtherBody() instanceof Bullet) e.getOtherBody().destroy();
		}
	}

}
