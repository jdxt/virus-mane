package game;

import city.cs.engine.*;

import javax.swing.*;
import java.awt.event.*;

public class Enemy extends Player {
	private final Shooter shooter;
	//private GameWorld world;
	private Timer timer;

	Enemy(GameLevel world, Gun gun) {
		super(world, gun);
		shooter = new Shooter();
		// world.addStepListener(shooter);
		timer = new Timer(1000, new Shoot());
	}

	public void destroy() {
		this.getWorld().removeStepListener(shooter);
		super.destroy();
	}

	public void startWalking() {
		super.startWalking(-1);
	}

	public void startShooting() {
		timer.start();
	}

	public void stopShooting() {
		timer.stop();
	}

	public boolean isShooting() {
		return this.timer.isRunning();
	}

	class Shoot implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Enemy.this.shoot();
		}
	}

	private class Shooter implements StepListener {
		private byte steps = 0;

		@Override
		public void preStep(StepEvent stepEvent) {
			steps++;
			steps %= 64;
		}

		@Override
		public void postStep(StepEvent stepEvent) {
			if (this.steps == 0) Enemy.this.shoot();
		}
	}
}
