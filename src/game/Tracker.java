package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

/**
 * Pan the view to follow a particular body.
 */
public class Tracker implements StepListener {
	/** The view */
	private WorldView view;

	/** The body */
	private Player player;

	public Tracker(WorldView view, Player player) {
		this.view = view;
		this.player = player;
	}

	@Override
	public void preStep(StepEvent e) {
	}

	@Override
	public void postStep(StepEvent e) {
		float px = player.getPosition().x;
		if (px < 100) view.setCentre(new Vec2(px + 5, 10));
		else view.setCentre(new Vec2(px, 0));
	}

}
