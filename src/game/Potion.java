package game;

import city.cs.engine.*;
import org.jbox2d.common.*;

import static org.jbox2d.common.MathUtils.*;

public class Potion extends StaticBody {
	private static final Shape s = new BoxShape(1.3f, 1.3f);
	private static final BodyImage img = new BodyImage("img/sprite.png");

	private float t = 0;
	private float sprite_num = 0;
	private Vec2 img_offset = new Vec2(-64, 0);
	private AttachedImage sprite;
	private Player player;

	Potion(GameLevel world, Player p) {
		super(world);
		this.player = p;
		this.setClipped(true);
		sprite = new AttachedImage(this, img, 2.5f,0, img_offset);
		this.getWorld().addStepListener(new Mover());
		new GhostlyFixture(this, s);
	}

	public boolean intersects() {
		return super.intersects(player);
	}

	class Mover implements StepListener {
		@Override
		public void preStep(StepEvent e) {
			sprite_num += 1f/12;
			sprite_num %= 5;
			sprite.setOffset(new Vec2((int)sprite_num*2.5f-5,0));

			if (Potion.this.intersects()) {
				Potion.this.player.heal();
				Potion.this.destroy();
			}
		}

		@Override
		public void postStep(StepEvent e) {
			Potion.this.t += e.getStep();
			Vec2 pos = Potion.this.getPosition();
			float s = sin(4 * t);
			Potion.this.setPosition(new Vec2(pos.x, 2 + s/2));
		}
	}
}
