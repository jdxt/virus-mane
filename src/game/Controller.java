package game;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Key handler to control a Walker.
 */
public class Controller extends KeyAdapter {
	private Player player;

	/**
	 * @param player
	 * 		player to be controlled.
	 */
	public Controller(Player player) {
		this.player = player;
	}

	/**
	 * Handle key press events for walking and jumping.
	 *
	 * @param e
	 * 		description of the key event.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e == null) return;

		switch (e.getKeyCode()) {
			case KeyEvent.VK_Q:
				System.exit(0);
			case KeyEvent.VK_A:
			case KeyEvent.VK_LEFT:
				Key.Left.on();
				player.key(Key.Left);
				break;
			case KeyEvent.VK_D:
			case KeyEvent.VK_RIGHT:
				Key.Right.on();
				player.key(Key.Right);
				break;
			case KeyEvent.VK_W:
			case KeyEvent.VK_UP:
				player.jump();
				break;
			case KeyEvent.VK_S:
			case KeyEvent.VK_DOWN:
				player.duck();
				break;
			case KeyEvent.VK_SPACE:
			case KeyEvent.VK_ENTER:
				player.shoot();
				break;
		}
	}

	/**
	 * Handle key release events (stop walking).
	 *
	 * @param e
	 * 		description of the key event.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		if (e == null) return;
		switch (e.getKeyCode()) {
			case KeyEvent.VK_A:
			case KeyEvent.VK_LEFT:
				Key.Left.off();
				break;
			case KeyEvent.VK_D:
			case KeyEvent.VK_RIGHT:
				Key.Right.off();
				break;
			case KeyEvent.VK_SPACE:
				player.stopShooting();
				break;
		}

		boolean on = Key.Left.isDown() || Key.Right.isDown();
		if (!on) player.stopWalking();
	}

	public enum Key {
		Left,
		Right;

		public boolean down = false;

		public void on() {
			this.down = true;
		}

		public void off() {
			this.down = false;
		}

		public boolean isDown() {
			return this.down;
		}
	}
}
