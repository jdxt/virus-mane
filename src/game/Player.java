package game;

import city.cs.engine.Shape;
import city.cs.engine.*;
import org.jbox2d.common.*;

import java.awt.*;

/**
 * Simple character
 */
public class Player extends Walker {
	private static final float JUMPING_SPEED = 30;
	private static final float WALKING_SPEED = 4;

	private static final float width = 2f;
	private static final float height = 4f;
	private static final Shape shape = new BoxShape(width / 2, height / 2);
	//private static final BodyImage image = new BodyImage("data/yellow-bird.gif");

	/** Represents different weapons */
	enum Gun {
		Pistol,
		AR,
		MG,
		SMG;

		public int getFireRate() {
			switch (this) {
				case SMG:
					return 7;
				case AR:
				case Pistol:
					return 5;
				case MG:
				default:
					return 3;
			}
		}

		public FireMode getFireMode() {
			switch (this) {
				case AR:
					return FireMode.Burst;
				case SMG:
				case MG:
					return FireMode.Auto;
				case Pistol:
				default:
					return FireMode.Semi;
			}
		}

		public int getDamage() {
			switch (this) {
				case AR:
				case MG:
					return 2;
				case Pistol:
				case SMG:
				default:
					return 1;
			}
		}

		public int getMaxBullets() {
			switch (this.getFireMode()) {
				case Burst:
					return 3;
				case Auto:
					return Integer.MAX_VALUE;
				case Semi:
				default:
					return 1;
			}

		}
	}

	enum FireMode {
		Semi,
		Burst,
		Auto
	}


	private final GameLevel world;
	private final Gun gun;
	private int health = 10;
	private Direction direction = Direction.Right;

	public Player(GameLevel world, Gun gun) {
		super(world, shape);
		this.world = world;
		// TODO: set gun from level
		this.gun = gun;
		//addImage(image);

		this.setFillColor(new Color(1f,0,0,1f));
		world.addStepListener(new ShotListener());
	}

	public static float getHeight() {
		return height;
	}

	public static float getWidth() {
		return width;
	}

	public void destroy() {
		// TODO: end screen
		super.destroy();
	}

	@Override
	public void startWalking(float speed) {
		this.stopWalking();
		float posX = this.getPosition().x;
		if (posX < 0 && speed < 0) return;

		this.direction = speed < 0 ? Direction.Left : Direction.Right;
		super.startWalking(speed);
	}


	private boolean shooting = false;
	public void shoot() {
		shooting = true;
	}

	public void stopShooting() {
		shooting = false;
	}

	class ShotListener implements StepListener {
		private final float rate = 1f / Player.this.gun.getFireRate();
		private final int maxShots = Player.this.gun.getMaxBullets();

		private float lastShot = 0;
		private int n_shot = 0;

		@Override
		public void preStep(StepEvent e) {
			lastShot += e.getStep();
			if (!Player.this.shooting) {
				n_shot = 0;
			} else if (lastShot >= rate) {
				new Bullet(Player.this, Player.this.gun.getDamage(), n_shot);
				lastShot = 0;
				n_shot++;
			}

			if (n_shot >= maxShots) {
				n_shot = 0;
				Player.this.shooting = false;
			}
		}

		@Override
		public void postStep(StepEvent e) {}
	}

	public Direction getDirection() {
		return this.direction;
	}

	public void jump() {
		super.jump(JUMPING_SPEED);
	}

	public void duck() {
		float x = this.getLinearVelocity().x;
		this.setLinearVelocity(new Vec2(x, -JUMPING_SPEED * 2));
	}

	public void key(Controller.Key k) {
		switch (k) {
			case Left:
				this.startWalking(-WALKING_SPEED);
				break;
			case Right:
				this.startWalking(WALKING_SPEED);
				break;
		}
		for (Controller.Key key : Controller.Key.values()) {
			if (key.isDown()) return;
		}
		this.stopWalking();
	}

	public void heal() {
		this.health++;
		if (health > 10) health = 10;
		this.setFillColor(new Color(1f,0,0,this.health/10f));
	}

	public void hit(int damage) {
		this.health -= damage;
		this.setFillColor(new Color(1f,0,0,this.health/10f));
	}

	public int getHealth() {
		return this.health;
	}

	public enum Direction {
		Left,
		Right
	}
}
