package game;

import city.cs.engine.*;

import org.jbox2d.common.Vec2;

public class Collision implements CollisionListener {
	private final Player player;

	Collision(Player player) {
		this.player = player;
	}

	@Override
	public void collide(CollisionEvent e) {
		Body other = e.getOtherBody();
		if (e.getReportingBody() == this.player) {
			if (other instanceof Enemy) {
				this.player.hit(1);
				this.player.stopWalking();
				this.player.setLinearVelocity(new Vec2(-50, 2));
			} else if (other instanceof Bullet) {
				this.player.hit(((Bullet) other).getDamage());
				other.destroy();
			} else if (other instanceof Potion) {
				this.player.heal();
				other.destroy();
			}
		}

		if (this.player.getHealth() <= 0) {
			this.player.destroy();
			System.out.println("its dead");
		}
	}
}
