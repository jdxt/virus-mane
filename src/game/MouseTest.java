package game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Dummy mouse listener.
 */
public class MouseTest implements MouseListener {
	private Player player;

	MouseTest(Player player) {
		super();
		this.player = player;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("mouse clicked");
		this.player.shoot();
		System.out.println(e.getComponent());
		System.out.println(this.player.getPosition());
		System.out.println(e.getPoint());
		System.out.println(e.getLocationOnScreen());
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
